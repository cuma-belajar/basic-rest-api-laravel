<?php

namespace App\Http\Controllers;

use App\Models\Book;

use App\Exceptions\ErrorException;
use App\Exceptions\FailException;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Validator;

class BookController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        /* Get All Book Data */
        $books = Book::select('id','name','publisher')->get();

        return response([
            "status" => "success",
            "data" => [
                "books" => $books
            ]
        ], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = $this->formValidate($request);

        if ($validator->fails()) {
            $errors = $validator->errors()->toArray();

            if(isset($errors['name']))
                throw new FailException("Gagal menambahkan buku. Mohon isi nama buku");

            if(isset($errors['readPage']))
                throw new FailException("Gagal menambahkan buku. readPage tidak boleh lebih besar dari pageCount");

            throw new ErrorException("Buku gagal ditambahkan");
        }

        // Retrieve the validated input...
        $validated = $validator->validated();
        $validated['id'] = Str::random(16);

        try {
            $book = Book::create($validated);

            return response([
                "status" => "success",
                "message" => "Buku berhasil ditambahkan",
                "data" => [
                    "bookId" => $book->id
                ]
            ], 200);
        } catch (\Throwable $th) {
            throw new ErrorException("Buku gagal ditambahkan");
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $book = Book::selectRaw("id, name, year, author, summary, publisher, pageCount, readPage, IF(pageCount=readPage, TRUE, FALSE) AS finished, reading, insertedAt, updatedAt")
        ->where('id', $id)->firstOr(function() {
            throw new ErrorException("Buku tidak ditemukan");
        });

        return response([
            "status" => "success",
            "data" => [
                "book" => $book
            ]
        ], 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = $this->formValidate($request);

        if ($validator->fails()) {
            $errors = $validator->errors()->toArray();

            if(isset($errors['name']))
                throw new FailException("Gagal memperbarui buku. Mohon isi nama buku");

            if(isset($errors['readPage']))
                throw new FailException("Gagal memperbarui buku. readPage tidak boleh lebih besar dari pageCount");

            throw new ErrorException("Buku gagal diperbarui");
        }

        // Retrieve the validated input...
        $validated = $validator->validated();

        $book = Book::where('id', $id)->firstOr(function() {
            throw new FailException("Gagal memperbarui buku. Id tidak ditemukan", 404);
        });

        try {
            $book->update($validated);

            return response([
                "status" => "success",
                "message" => "Buku berhasil diperbarui"
            ], 200);
        } catch (\Throwable $th) {
            throw new ErrorException("Buku gagal diperbarui");
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $book = Book::where('id', $id)->firstOr(function() {
                throw new FailException("Buku gagal dihapus. Id tidak ditemukan", 404);
            });

            $book->delete();

            return response([
                "status" => "success",
                "message" => "Buku berhasil dihapus"
            ], 200);
        } catch (\Throwable $th) {
            throw new ErrorException("Terjadi Kesalahan, Buku Gagal Dihapus!");
        }
    }

    /**
     * Validating Input.
     *
     * @param  Request $request
     * @return \Illuminate\Support\Facades\Validator
     */
    public function formValidate(Request $request)
    {
        return Validator::make($request->all(), [
            'name' => 'required|max:255',
            'year' => 'required|integer|min:1970|max:3000',
            'author' => 'required',
            'summary' => 'required',
            'publisher' => 'required',
            'pageCount' => 'required|integer',
            'readPage' => 'required|integer|lte:pageCount',
            'reading' => 'required|boolean',
        ]);
    }
}
