<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'id',
        'name',
        'year',
        'author',
        'summary',
        'publisher',
        'pageCount',
        'readPage',
        'reading',
    ];

    public $incrementing = false;
    protected $keyType = 'string';

    const CREATED_AT = 'insertedAt';
    const UPDATED_AT = 'updatedAt';

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'finished' => 'boolean',
        'reading' => 'boolean',
    ];
}
